<?php

/**
 * @file
 * Admin forms and functinality for Amazon Header Bidding.
 */

/**
 * Form builder for the Amazon Header Bidding settings form.
 */
function amazon_hb_admin_settings($form, $form_state) {
  $form['#submit'][] = 'amazon_hb_admin_settings_submit';

  $form['amazon_hb_pub_id'] = array(
    '#type' => 'textfield',
    '#title' => t('PubID'),
    '#default_value' => variable_get('amazon_hb_pub_id', ''),
    '#required' => TRUE,
    '#description' => t('The Publisher ID.'),
  );

  return system_settings_form($form);
}

function amazon_hb_admin_settings_submit($form, $form_state) {
  // Check dfp has been set.
  if (empty(variable_get('dfp_network_id', ''))) {
    $vars = array(
      '!dfp_settings_link' => l(t('DFP settings'), 'admin/structure/dfp_ads/settings'),
    );
    drupal_set_message(
      t('Please configure !dfp_settings_link', $vars),
      'warning'
    );
  }
}
